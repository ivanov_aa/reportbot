﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ReportBotLibrary.Services
{
    public class ReportService
    {
        private HttpClient HttpClient { get; set; }

        public ReportService(string baseAddress, string header)
        {
            this.HttpClient = CreateHttpClient(baseAddress, header);
        }

        public void Post(string uri, object ring)
        {
            this.HttpClient.PostAsync(uri, new StringContent(JsonConvert.SerializeObject(ring), Encoding.UTF8, "application/json"));
        }

        public async Task<T> Get<T>(IDictionary<string, object> args = null)
        {
            var parametrs = SerializeParameters(args);
            var response = await this.HttpClient.GetAsync(parametrs);
            var str = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(str);
        }

        public async Task<string> GetString(string uri)
        {
            return await this.HttpClient.GetStringAsync(uri);
        }

        public async Task<byte[]> GetBytesAsync(string uri)
        {
            return await this.HttpClient.GetByteArrayAsync(uri);
        }

        private static HttpClient CreateHttpClient(string uri, string header)
        {
            var result = new HttpClient(new HttpClientHandler() { UseDefaultCredentials = true })
            {
                BaseAddress = new Uri(uri)
            };
            result.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(header));

            return result;
        }

        private static string SerializeParameters(IDictionary<string, object> parameters)
        {
            return parameters == null ? null : "?" + string.Join("&", parameters.Keys.Select(x => x + "=" + parameters[x]));

        }
    }
}
