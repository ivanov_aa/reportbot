﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ReportBotLibrary.DbAccess
{
    public abstract class DbAccess
    {
        protected IDbConnection Connection;

        protected void InitializeConnection(string cnn)
        {
            this.Connection = new SqlConnection(cnn);
            this.Connection.Open();
        }
    }
}