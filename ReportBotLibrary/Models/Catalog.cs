﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ReportBotLibrary.Models
{
    public class Catalog
    {
        [JsonProperty("@odata.context")]
        public string Context { get; set; }
        [JsonProperty("value")]
        public List<CatalogItem> Items { get; set; }
    }
    public class CatalogItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public object Description { get; set; }
        public string Path { get; set; }
        public string Type { get; set; }
        public bool Hidden { get; set; }
        public int Size { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ParentFolderId { get; set; }
        public string ContentType { get; set; }
        public string Content { get; set; }
        public bool IsFavorite { get; set; }
        public List<object> Roles { get; set; }
        public bool HasDataSources { get; set; }
        public bool HasSharedDataSets { get; set; }
        public bool HasParameters { get; set; }
    }
}
