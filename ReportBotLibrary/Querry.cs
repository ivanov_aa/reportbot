﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ReportBotLibrary
{
    public class Querry
    {
        private static readonly HttpClient HttpClient = CreateHttpClient();
        private static string _uri;

        public Querry(string uri)
        {
            _uri = uri;
        }

        public void Post(string uri, object obj)
        {
            HttpClient.PostAsync(uri, new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json"));
        }

        public async Task<string> Get(string uri)
        {
            var response = await HttpClient.GetAsync(uri);
            var content = await response.Content.ReadAsStringAsync();
            return content;
        }

        private static HttpClient CreateHttpClient()
        {
            var result = new HttpClient(new HttpClientHandler() { UseDefaultCredentials = true })
            {
                BaseAddress = new Uri(_uri)
            };
            result.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return result;
        }
    }
}
