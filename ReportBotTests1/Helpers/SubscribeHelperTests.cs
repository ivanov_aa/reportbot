﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReportBot.Helpers;

namespace ReportBotTests.Helpers
{
    [TestClass()]
    public class SubscribeHelperTests
    {
        [TestMethod()]
        public void IsNowTimeForSendTest()
        {
            var res = SubscribeHelper.IsNowTimeForSend(12, 4);
            Assert.AreEqual(true, res);
        }
    }
}