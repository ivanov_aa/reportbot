﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Autofac;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Internals;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;
using ReportBot.DataAccess.Db;
using ReportBot.DataAccess.Service;
using ReportBot.Dialogs;
using ReportBot.Models;

namespace ReportBot.Helpers
{
    public class SubscribeHelper
    {
        public static async void Send()
        {
            var db = new DbQueries();
            var subscribes = await db.GetSubscribes();

            foreach (var subscribe in subscribes)
            {
                try
                {
                    if (IsNowTimeForSend(subscribe.STime.Hour, subscribe.SendPeriod))
                    {
                        var oldMessage = GetMessage(subscribe.ConversationReference);

                        if (!oldMessage.ChannelId.Contains("emulator"))
                        {
                            var reportBytes = await GetReport(subscribe.ReportID);
                            await SendReportToSubscriber(oldMessage, reportBytes);
                        }
                    }
                }
                catch (Exception e)
                {
                }
            }
        }

        private static async Task<byte[]> GetReport(Guid reportId)
        {
            var db = new DbQueries();
            var rep = new HttpQueries("application/pdf");
            var reportName = await db.GetReportNameByIdAsync(reportId);
            return await rep.GetReport(reportName);
        }

        public static bool IsNowTimeForSend(int sHour, int period)
        {
            var nowHour = DateTime.Now.Hour;
            var r = (nowHour - sHour) % period;
            return r == 0;
        }

        private static Activity GetMessage(string reference)
        {
            var conversationReference = UrlToken.Decode<ConversationReference>(reference);
            // Recreate the message from the conversation reference that was saved previously.
            return conversationReference.GetPostToBotMessage();
        }

        private static async Task SendReportToSubscriber(Activity oldMessage, byte[] reportBytes)
        {
            var userAccount = new ChannelAccount(oldMessage.From.Id, oldMessage.From.Name);
            var botAccount = new ChannelAccount(oldMessage.Recipient.Id, oldMessage.Recipient.Name);
            var connector = new ConnectorClient(new Uri(oldMessage.ServiceUrl));

            var conversationId = oldMessage.Conversation.Id;
            var message = Activity.CreateMessageActivity();

            if (!string.IsNullOrEmpty(conversationId) && !string.IsNullOrEmpty(oldMessage.ChannelId))
                message.ChannelId = oldMessage.ChannelId;
            else
                conversationId = (await connector.Conversations.CreateDirectConversationAsync(botAccount, userAccount)).Id;

            message.From = botAccount;
            message.Recipient = userAccount;
            message.Conversation = new ConversationAccount(id: conversationId);
            message.Text = "Ваш отчёт по подписке:";
            message.AttachmentLayout = AttachmentLayoutTypes.List;
            message.Attachments = reportBytes.GetPdfAttachmentList();
            message.Locale = "en-Us";
            await connector.Conversations.SendToConversationAsync((Activity)message);
        }
    }
}