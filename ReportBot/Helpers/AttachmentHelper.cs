﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Microsoft.Bot.Connector;

namespace ReportBot.Helpers
{
    public static class AttachmentHelper
    {
        private static readonly string RsServer = ConfigurationManager.AppSettings["ServerName"];

        public static IList<Attachment> ToAttachmentList(this IEnumerable<string> items)
        {
            var attachments = new List<Attachment>();
            var actions = items.Select(path => new CardAction(ActionTypes.ImBack, title: path, value:path)).ToList();

            var heroCard = new HeroCard { Buttons = actions };
            attachments.Add(heroCard.ToAttachment());

            return attachments;
        }

        public static IList<Attachment> GetPdfAttachmentList(this byte[] reportBytes)
        {
            var attachments = new List<Attachment>();
            var contentType = "application/pdf";
            var attachment = new Attachment
            {
                Name = "report.pdf",
                ContentUrl = $"data:{contentType};base64,{Convert.ToBase64String(reportBytes)}",
                ContentType = contentType
            };
            attachments.Add(attachment);

            return attachments;
        }
    }
}