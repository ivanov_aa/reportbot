﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportBot.DataAccess.Service
{
    public interface IHttpQueries
    {
        object GetReports();
    }
}
