﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace ReportBot.DataAccess.Service
{
    public class HttpQueries : IHttpQueries
    {
        private HttpClient _http;

        public HttpQueries(string header)
        {
            _http = CreateHttpClient(header);
        }

        public object GetReports()
        {
            throw new NotImplementedException();
        }

        public async Task<byte[]> GetReport(string reportName)
        {
            var str = $"?{reportName}&rs:Format=PDF";
            return await _http.GetByteArrayAsync(str);
        }

        private static HttpClient CreateHttpClient(string header)
        {
            var result = new HttpClient(new HttpClientHandler() { UseDefaultCredentials = true })
            {
                BaseAddress = new Uri(ConfigurationManager.AppSettings["ServerName"])
            };
            result.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(header));

            return result;
        }
    }
}