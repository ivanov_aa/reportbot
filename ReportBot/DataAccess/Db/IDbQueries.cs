﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Bot.Connector;
using ReportBot.Models;

namespace ReportBot.DataAccess.Db
{
    public interface IDbQueries
    {
        Task<IEnumerable<string>> GetReportsNamesAsync();
        Task<string> GetReportNameByIdAsync(Guid reportId);
        Task<bool> UserExist(string channelId, string userId);
        Task<IEnumerable<Guid>> GetUserIdByPhoneAsync(string phone);
        Task Subscribe(string reportName, string channelId, string userId, ConversationReference conversationReference);
        Task<IEnumerable<SubscribeModel>> GetSubscribes();
    }
}
