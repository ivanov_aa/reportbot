﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using ReportBot.Models;

namespace ReportBot.DataAccess.Db
{
    public class DbQueries : ReportBotLibrary.DbAccess.DbAccess, IDbQueries
    {
        public DbQueries()
        {
            base.InitializeConnection(ConfigurationManager.AppSettings["ConnectionString"]);
        }

        public async Task<IEnumerable<string>> GetReportsNamesAsync()
        {
            return await this.Connection.QueryAsync<string>("SELECT [Path] FROM [dbo].[Catalog] WHERE [Hidden] = 0 AND [Type] = 2");
        }

        public async Task<bool> UserExist(string channelId, string userId) //TODO; реализовать метод
        {
            var ids = await this.Connection.QueryAsync<Guid>(
                @"SELECT [UserID] 
                                    FROM [dbo].[MessangerUsers] 
                                    WHERE [ChannelID] = @channelId AND [ChannelUserID] = @userId"
                , new { channelId, userId });
            return ids.Any();
        }

        public async Task Subscribe(string reportName, string channelId, string userId, ConversationReference conversationReference)
        {
            var reportId = await GetReportId(reportName);
            string reference = UrlToken.Encode(conversationReference); //TODO: Сохранить ссылку на диалог в БД

            await this.Connection.ExecuteAsync(@"INSERT INTO [dbo].[ChannelsSubscibes]
                                                       ([ReportID]
                                                       ,[ChannelID]
                                                       ,[ChannelUserID]
                                                       ,STime
                                                       ,ConversationReference)
                                                 VALUES
                                                       (@reportId
                                                       ,@channelId
                                                       ,@userId 
                                                       ,GETDATE()
                                                       ,@reference)",
                new { reportId, channelId, userId, reference });
        }

        private async Task<Guid> GetReportId(string reportName)
        {
            var res = await this.Connection.QueryAsync<Guid>("SELECT [ItemID] FROM [dbo].[Catalog] WHERE [Path] = @reportName", new { reportName });
            return res.FirstOrDefault();
        }

        public async Task CreateAccountAsync(Activity message)
        {
            var phone = message?.Text;
            var channelId = message.ChannelId;
            var channelUserId = message.From.Id;
            var ids = await GetUserIdByPhoneAsync(phone);

            await this.Connection.ExecuteAsync(@"INSERT INTO [dbo].[MessangerUsers]
                                                       ([UserID]
                                                       ,[ChannelID]
                                                       ,[ChannelUserID])
                                                 VALUES
                                                       (@userId
                                                       ,@channelId
                                                       ,@channelUserId)", new { userId = ids.FirstOrDefault(), channelId, channelUserId });
        }

        public async Task<IEnumerable<Guid>> GetUserIdByPhoneAsync(string phone)
        {
            return await this.Connection.QueryAsync<Guid>("SELECT [UserID] FROM [dbo].[UserContactInfo] WHERE [Phone] = @phone", new { phone });
        }

        public async Task<IEnumerable<SubscribeModel>> GetSubscribes()
        {
            return await this.Connection.QueryAsync<SubscribeModel>(
                @"SELECT [ReportID]
      ,[ChannelID]
      ,[ChannelUserID]
      ,[STime]
      ,[ETime]
      ,[SendPeriod]
      ,[ConversationReference]
  FROM [ReportServer].[dbo].[ChannelsSubscibes]
  WHERE ConversationReference != '' AND SendPeriod != 0
  AND ([ETime] IS Null OR [ETime] < GETDATE())
  AND [ChannelID] != 'emulator'
");
        }

        public async Task<string> GetReportNameByIdAsync(Guid reportId)
        {
            var res = await this.Connection.QueryAsync<string>("SELECT [Path] FROM [dbo].[Catalog] WHERE [ItemID] = @reportId", new { reportId });
            return res.FirstOrDefault();
        }
    }
}