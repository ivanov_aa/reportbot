﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportBot.Models
{
    public class SubscribeModel
    {
        public Guid ReportID { get; set; }
        public string ChannelID { get; set; }
        public string ChannelUserID { get; set; }
        public DateTime STime { get; set; }
        public DateTime ETime { get; set; }
        public string ConversationReference { get; set; }
        /// <summary>
        /// Период отправки отчёта в часах
        /// </summary>
        public int SendPeriod { get; set; }
    }
}