﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using ReportBot.DataAccess.Db;
using ReportBot.Helpers;

namespace ReportBot.Dialogs
{
    [Serializable]
    internal class AuthDialog : IDialog<object>
    {
        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result as Activity;
            var phone = message?.Text;
            if(Verify(phone))
            { 
            if (await this.AdAccountExist(phone))
            {
                await CreateUserAccount(message);
                await context.PostAsync($"Регистрация по номеру { phone } успешно пройдена!");
                await ShowOMenu(context, result);
                context.Call(new MenuDialog(), this.ReportDialogResumeAfter);
                }
            else
            {
                await context.PostAsync($"Авторизация по номеру {phone} не пройдена! Не найден в AD аккаунт соответствующий вашему номеру телефона.");
                context.Done("Доступ запрещён!");
            }
            }
            else
            {
                await context.PostAsync($"Введён не верный номер телефона: {phone}, повторите пожалуйста ввод.");
                context.Wait(MessageReceivedAsync);
            }
        }
        private async Task ShowOMenu(IDialogContext context, IAwaitable<object> result) //TODO: Перенести в статический класс - хелпер
        {
            //var rep = new DbQueries();
            var buttons = new List<string>() { "Список отчётов", "Подписаться на отчёт" };

            var reply = context.MakeMessage();
            reply.Text = $"Меню:";
            reply.AttachmentLayout = AttachmentLayoutTypes.List;
            reply.Attachments = buttons.ToAttachmentList();

            await context.PostAsync(reply);
            //context.Wait(ShowReport);
        }

        private bool Verify(string phone)
        {
            var numericPhone = new string(phone.ToCharArray().Where(char.IsDigit).ToArray());
            return numericPhone.Length == 11;
        }

        private async Task CreateUserAccount(Activity message)
        {
            var rep = new DbQueries();
            await rep.CreateAccountAsync(message);
        }

        private async Task ReportDialogResumeAfter(IDialogContext context, IAwaitable<object> result)
        {
            try
            {
                var resultFromReportDialog = await result;
                //await context.PostAsync($"Спасибо за ипользование нашего сервиса! Надеюсь Вам понравилось)).");
                context.Done("You forwarded to previous dialog from MenuDialog");
            }
            catch (Exception e)
            {
                await context.PostAsync($"Простите. я не понял Вас. Давайте попробуем снова. EXCEPTION: {e.Message}. StackTrace: {e.StackTrace}");
            }
        }

        private async Task<bool> AdAccountExist(string phone)
        {
            var rep = new DbQueries();
            var ids = await rep.GetUserIdByPhoneAsync(phone); //TODO: Реализовать метод
            return ids.Any();
        }
    }
}