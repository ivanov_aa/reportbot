﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using ReportBot.DataAccess.Db;
using ReportBot.Helpers;

namespace ReportBot.Dialogs
{
    [Serializable]
    public class RootDialog : IDialog<object>
    {
        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var message = await result as Activity;
            var user = message.From;
            //try
            //{
            //    await SubscribeHelper.Resume();
            //}
            //catch (Exception e)
            //{
            //    await context.PostAsync($@"Exc: {e.Message}, stacktrase: {e.StackTrace}");
            //}
            if (await UserExist(message.ChannelId, user.Id))
            {
                await context.PostAsync($@"Добро пожаловать {user.Name}! 
                                           Вас приветствует сервис отчётов.");
                await ShowOMenu(context, result);
                context.Call(new MenuDialog(), this.ReportDialogResumeAfter);
            }
            else
            {
                await context.PostAsync($@"{message.From.Name}, Вы незарегистрированы в системе. 
                                           Для регистрации отправьте свой номер мобильного телефона в ответном сообщении.");
                context.Call(new AuthDialog(), this.ReportDialogResumeAfter);
            }
        }

        private async Task ShowOMenu(IDialogContext context, IAwaitable<object> result)
        {
            //var rep = new DbQueries();
            var buttons = new List<string>(){"Список отчётов", "Подписаться на отчёт"};

            var reply = context.MakeMessage();
            reply.Text = $"Меню:";
            reply.AttachmentLayout = AttachmentLayoutTypes.List;
            reply.Attachments = buttons.ToAttachmentList();

            await context.PostAsync(reply);
            //context.Wait(ShowReport);
        }

        private async Task<bool> UserExist(string channelId, string userId)
        {
            var rep = new DbQueries();
           return await rep.UserExist(channelId, userId);
        }

        private async Task ReportDialogResumeAfter(IDialogContext context, IAwaitable<object> result)
        {
            try
            {
                var resultFromReportDialog = await result;
                await context.PostAsync($"Спасибо за ипользование нашего сервиса! Надеюсь Вам понравилось)).");
            }
            catch (Exception e)
            {
                await context.PostAsync($"Простите. я не понял Вас. Давайте попробуем снова. EXCEPTION: {e.Message}. StackTrace: {e.StackTrace}");
            }
        }
    }
}