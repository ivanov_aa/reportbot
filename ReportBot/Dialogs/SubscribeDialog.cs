﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.ConnectorEx;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using ReportBot.DataAccess.Db;

namespace ReportBot.Dialogs
{
    [Serializable]
    public class SubscribeDialog : IDialog<object>
    {
        //[NonSerialized]
        //private readonly DbQueries _rep;

        //public SubscribeDialog()
        //{
        //    this._rep = new DbQueries(); 
        //}

        public Task StartAsync(IDialogContext context)
        {
            //rep = new DbQueries();

            context.Wait(MessageReceivedAsync);
            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var message = await result as Activity;
            var reportName = message.Text;

            var rep = new DbQueries();
            var reports = await rep.GetReportsNamesAsync();
            var name = reports.FirstOrDefault(x => x.Contains(reportName));

            if (!string.IsNullOrWhiteSpace(name))
            {
                ConversationReference conversationReference = message.ToConversationReference();
                await rep.Subscribe(reportName, message.ChannelId, message.From.Id, conversationReference);
                await context.PostAsync($"Подписка на отчёт {name} успешно создана!");
                context.Done("You forwarded to previous dialog from SubscribeDialog");
            }
            else
            {
                await context.PostAsync("Неверный ввод. Выберите пожалуйста отчёт для создания подписки");
                context.Wait(MessageReceivedAsync);
            }
        }
    }
}