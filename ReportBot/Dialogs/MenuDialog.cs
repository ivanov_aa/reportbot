﻿using System;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using ReportBot.DataAccess.Db;
using ReportBot.DataAccess.Service;
using ReportBot.Helpers;
using Task = System.Threading.Tasks.Task;

namespace ReportBot.Dialogs
{
    [Serializable]
    public class MenuDialog : IDialog<object>
    {
        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync); 
            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var message = await result as Activity;
            var text = message?.Text ?? string.Empty;

            switch (text.ToLowerInvariant())
            {
                case "список отчётов":
                    await ShowReportsList(context, result);
                    context.Call(new ReportDialog(), this.MenuDialogResumeAfter);
                    break;
                case "подписаться на отчёт":
                    await ShowReportsList(context, result);
                    context.Call(new SubscribeDialog(), this.MenuDialogResumeAfter);
                    break;
                default:
                    await context.PostAsync($"MenuDialog: Неверный ввод. Пожалуйста выберите верный пункт меню");
                    context.Wait(MessageReceivedAsync);
                    break;
            }

        }

        private async Task MenuDialogResumeAfter(IDialogContext context, IAwaitable<object> result)
        {
            try
            {
                var resultFromReportDialog = await result;
                //await context.PostAsync($"MenuDialog: Спасибо за ипользование нашего сервиса! Надеюсь Вам понравилось)).");
                context.Done("You forwarded to previous dialog from MenuDialog");
            }
            catch (Exception e)
            {
                await context.PostAsync($"Простите. я не понял Вас. Давайте попробуем снова. EXCEPTION: {e.Message}. StackTrace: {e.StackTrace}");
            }
        }

        private async Task ShowReportsList(IDialogContext context, IAwaitable<object> result)
        {
            var rep = new DbQueries();
            var reports = await rep.GetReportsNamesAsync();

            var reply = context.MakeMessage();
            reply.Text = $"Список доступных отчётов:";
            reply.AttachmentLayout = AttachmentLayoutTypes.List;
            reply.Attachments = reports.ToAttachmentList(); 

            await context.PostAsync(reply);
        }
    }
}