﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Internals;
using Microsoft.Bot.Connector;
using ReportBot.DataAccess.Db;
using ReportBot.DataAccess.Service;
using ReportBot.Helpers;

namespace ReportBot.Dialogs
{
    [Serializable]
    public class ReportDialog : IDialog<object>
    {
        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);
            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var message = await result as Activity;
            var reportName = message?.Text ?? string.Empty;

            var db = new DbQueries();
            var reports = await db.GetReportsNamesAsync();
            var name = reports.FirstOrDefault(x => x.Contains(reportName));

            if (!string.IsNullOrWhiteSpace(name))
            {
                var reply = await GetReportForSend(context, name);

                await context.PostAsync(reply);
                context.Done("You forwarded to previous dialog from ReportDialog");
            }
            else
            {
                await context.PostAsync("Неверный ввод. Выберите пожалуйста отчёт для просмотра");
                context.Wait(MessageReceivedAsync);
            }
        }

        private async Task<IMessageActivity> GetReportForSend(IBotToUser context, string name)
        {
            var rep = new HttpQueries("application/pdf");
            var reportBytes = await rep.GetReport(name);

            var reply = context.MakeMessage();
            reply.Text = $"Ваш отчёт {name}";
            reply.AttachmentLayout = AttachmentLayoutTypes.List;
            reply.Attachments = reportBytes.GetPdfAttachmentList();
            return reply;
        }
    }
}