﻿using System;
using System.Configuration;
using System.Timers;
using ReportBot.Helpers;

namespace ReportBot
{
    public class Subscriber : IDisposable //Синглтон паттерн
    {
        /// <summary>
        /// Текущий и единственный экземпляр класса 
        /// </summary>
        public static Subscriber Current { get; private set; }
        /// <summary>
        /// Синхрообъект
        /// </summary>
        private static readonly object SyncObj = new object();
        private readonly Timer _timer;
        //private readonly double _reportSendingPeriod = Convert.ToDouble(ConfigurationManager.AppSettings["ReportSendingPeriod"]);
        private readonly double _reportSendingPeriod = TimeSpan.FromHours(1.0).TotalMilliseconds;
        //private readonly double _reportSendingPeriod = TimeSpan.FromMinutes(1.0).TotalMilliseconds;

        private Subscriber()
        { 
            this._timer = new Timer(_reportSendingPeriod);
            this._timer.Elapsed += SendSubscribes;
            this._timer.Start();
        }

        private void SendSubscribes(object sender, EventArgs e)
        {
            SubscribeHelper.Send();
        }

        public static void Create()
        {
            if (Current == null)
                lock (SyncObj)
                {
                    Current = new Subscriber();
                }
        }

        public void Dispose()
        {
        }
    }
}