﻿using System.Runtime.Versioning;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReportBot.Helpers;
using ReportBot.Models;

namespace ReportBotLibraryTests.Helpers
{
    [TestClass()]
    public class SubscribeHelperTests
    {
        [TestMethod()]
        public void IsNowTimeForSendTest()
        {
            var res = SubscribeHelper.IsNowTimeForSend(12, 12);
            Assert.IsTrue(res);
        }
    }
}