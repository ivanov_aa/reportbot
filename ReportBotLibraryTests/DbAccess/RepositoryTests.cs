﻿using System.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReportBot.DataAccess.Db;

namespace ReportBotLibraryTests.DbAccess
{
    [TestClass]
    public class RepositoryTests
    {
        [TestMethod]
        public void GetReportsTest()
        {
            var rep = new DbQueries();
            var res = rep.GetReportsNamesAsync();
            Assert.IsNotNull(res);
        }
    }
}